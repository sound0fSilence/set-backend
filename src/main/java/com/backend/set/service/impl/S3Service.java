package com.backend.set.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.backend.set.service.IS3Service;
import com.backend.set.utils.exception.AssessmentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.util.List;

@Service
public class S3Service implements IS3Service {

  @Autowired
  private AmazonS3 s3;

  @Override
  public List<String> getAllBucketNames() {
    return s3.listBuckets().stream().map(Bucket::getName).toList();
  }

  @Override
  public String createBucket(String bucketName) {
    return s3.createBucket(bucketName).getName();
  }

  @Override
  public StreamingResponseBody getFile(String bucketName, String fileName) {
    //reads the content from S3 bucket and returns a S3ObjectInputStream object
    S3Object object = s3.getObject(bucketName, fileName);
    S3ObjectInputStream finalObject = object.getObjectContent();
    return outputStream -> {
      int numberOFBitesToWrite = 0;
      byte[] data = new byte[1024];
      while ((numberOFBitesToWrite = finalObject.read(data, 0, data.length)) != -1) {
        outputStream.write(data, 0, numberOFBitesToWrite);
      }
      finalObject.close();
    };
  }

  @Override
  public String readFileAsString(String bucketName, String fileName) {
    S3Object object = s3.getObject(bucketName, fileName);
    S3ObjectInputStream objectContent = object.getObjectContent();
    String text;
    try {
      text = IOUtils.toString(objectContent);
      objectContent.close();
    } catch (IOException e) {
      throw new AssessmentException(String.format("No file found: %s in bucket: %s", bucketName, fileName));
    }
    return text;
  }

  @Override
  public void putFile(String bucketName, MultipartFile file) {
    try {
      ObjectMetadata metadata = new ObjectMetadata();
      metadata.setContentLength(IOUtils.toByteArray(file.getInputStream()).length);
      s3.putObject(new PutObjectRequest(bucketName, file.getOriginalFilename(), file.getInputStream(), metadata));
    } catch (IOException e) {
      throw new AssessmentException("Could not write file in bucket: " + bucketName);
    }
  }

  @Override
  public ListObjectsV2Result getFilesInBucket(String bucketName) {
    return s3.listObjectsV2(bucketName);
  }

  @Override
  public void deleteFile(String bucketName, String fileName) {
    s3.deleteObject(bucketName, fileName);
  }
}
