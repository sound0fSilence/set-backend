package com.backend.set.service.impl;

import com.backend.set.config.AssessmentPropertyConfig;
import com.backend.set.domain.dto.AssessmentDto;
import com.backend.set.domain.entity.Assessment;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.AssessmentMark;
import com.backend.set.repository.AssessmentRepository;
import com.backend.set.service.IAssessmentService;
import com.backend.set.service.IUserService;
import com.backend.set.utils.FrameworkUtils;
import com.backend.set.utils.exception.AssessmentException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
@Transactional
@Log4j2
public class AssessmentService implements IAssessmentService {

  @Autowired
  private AssessmentRepository assessmentRepository;
  @Autowired
  private IUserService userService;
  @Autowired
  private FrameworkUtils frameworkUtils;
  @Autowired
  private AssessmentPropertyConfig assessmentPropertyConfig;

  String greeting = """
      Starting assessment.
      Candidate - %s
      Experts: %s""";

  @Override
  public Assessment startAssessment(AssessmentDto assessmentDto) {
    ensureRequiredParticipants();
    log.info(String.format(greeting, userService.getCandidates().get(0), userService.getExperts()));
    Assessment assessment = new Assessment()
        .withExperts(userService.getExperts().stream().limit(2).toList())
        .withCandidate(userService.getCandidates().get(0))
        .withQuestions(frameworkUtils.getQuestionList())
        .withDate(assessmentDto.getDate())
        .withActive(true);
    return assessmentRepository.save(assessment);
  }

  @Override
  public double getTotalAssessmentMark(int assessmentId) {
    return getAssessmentMarks(assessmentId).stream().mapToInt(mark -> Integer.parseInt(mark.getMark())).average().getAsDouble();
  }

  @Override
  public List<AssessmentMark> getAssessmentMarks(int assessmentId) {
    List<AssessmentMark> marks = getAssessmentById(assessmentId).getMarks();
    if (marks.size() < 2) {
      throw new AssessmentException("Not all mandatory marks are set for the candidate. Please ask for experts");
    }
    return marks;
  }

  @Override
  public Assessment evaluateCandidate(int assessmentId, int expertId, AssessmentMark mark) {
    log.info(String.format("Got %s from %s", mark.getMark(), userService.getUserByID(expertId)));
    Assessment assessment = getAssessmentById(assessmentId);
    assessment.getMarks().add(AssessmentMark.decode(mark.getMark()));
    return assessmentRepository.save(assessment);
  }

  @Override
  public List<String> getQuestionList(int assessmentId) {
    return getAssessmentById(assessmentId).getQuestions();
  }

  @Override
  public String askRandomQuestion(int assessmentId) {
    Assessment assessment = getAssessmentById(assessmentId);
    if (assessment.getQuestions().isEmpty()) {
      return "There no questions left. Please, evaluate the candidate!";
    }
    int questionIndex = ThreadLocalRandom.current().nextInt(0, assessment.getQuestions().size());
    return assessment.getQuestions().remove(questionIndex);
  }

  public Assessment getAssessmentById(int assessmentId) {
    return assessmentRepository.findById(assessmentId).orElseThrow(() -> new AssessmentException("No such assessment found with id: " + assessmentId));
  }

  @Override
  public void deleteCandidate(Assessment assessment) {
    User user = userService.getUserByID(assessment.getCandidate().getId());
    userService.deleteUser(user);
  }

  @Override
  public List<Assessment> getAllAssessments() {
    return assessmentRepository.findAll();
  }

  @Override
  public Assessment getActiveAssessment() {
    return assessmentRepository.findAll().stream().filter(Assessment::isActive).findFirst()
        .orElseThrow(() -> new AssessmentException("No active assessment"));
  }

  @Override
  @SneakyThrows
  public void addQuestionList(String questions) {
    List<String> questionList = new ArrayList<>();
    new ObjectMapper().readValue(questions, JsonNode.class)
        .elements().forEachRemaining(entry -> questionList.add(entry.get("question").asText()));
    getActiveAssessment().getQuestions().addAll(questionList);
  }

  private void ensureRequiredParticipants() {
    if (userService.getExperts().size() < assessmentPropertyConfig.getRequiredMarkQty()) {
      throw new AssessmentException("It requires 2 experts for assessment, but was: " + userService.getExperts().size());
    } else if (userService.getCandidates().isEmpty()) {
      throw new AssessmentException("It requires one candidate for assessment, but was none");
    }
  }
}
