package com.backend.set.service.impl;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.repository.UserRepository;
import com.backend.set.service.IUserService;
import com.backend.set.utils.exception.UserException;
import com.backend.set.utils.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService implements IUserService {
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UserMapper userMapper;

  @Override
  public User registerNewUserAccount(UserDto userDto) throws UserException {
    validateUserExist(userDto);
    final User user = userMapper.dtoToEntity(userDto);
    return userRepository.save(user);
  }

  @Override
  public User saveUserAccount(User user) {
    return userRepository.save(user);
  }

  @Override
  public void deleteUser(final User user) {
    userRepository.delete(user);
  }

  @Override
  public User getUserByID(final int userId) {
    return userRepository.findById(userId)
        .orElseThrow(() -> new UserException("No user found with id: " + userId));
  }

  @Override
  public User getUserByEmail(final String email) {
    return Optional.ofNullable(userRepository.findByEmail(email))
        .orElseThrow(() -> new UserException("No user found with email: " + email));
  }

  @Override
  public List<User> getExperts() {
    return userRepository.findAll().stream().filter(user -> user.getRole().equals(Role.EXPERT)).toList();
  }

  @Override
  public List<User> getCandidates() {
    return userRepository.findAll().stream().filter(user -> user.getRole().equals(Role.CANDIDATE)).toList();
  }

  private void validateUserExist(UserDto userDto) {
    if (emailExists(userDto.getEmail())) {
      throw new UserException("There is an account with that email address: " + userDto.getEmail());
    }
  }

  private boolean emailExists(String email) {
    return userRepository.findByEmail(email) != null;
  }

}
