package com.backend.set.service;

import com.backend.set.domain.dto.AssessmentDto;
import com.backend.set.domain.entity.Assessment;
import com.backend.set.domain.enums.AssessmentMark;

import java.util.List;

public interface IAssessmentService {

  Assessment startAssessment(AssessmentDto assessmentDto);

  double getTotalAssessmentMark(int assessmentId);

  List<AssessmentMark> getAssessmentMarks(int assessmentId);

  Assessment evaluateCandidate(int assessmentId, int expertId, AssessmentMark mark);

  List<String> getQuestionList(int assessmentId);

  String askRandomQuestion(int assessmentId);

  Assessment getAssessmentById(int assessmentId);

  void deleteCandidate(Assessment assessment);

  List<Assessment> getAllAssessments();

  Assessment getActiveAssessment();

  void addQuestionList(String questions);
}
