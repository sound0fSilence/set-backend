package com.backend.set.service;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;

import java.util.List;

public interface IUserService {

  User registerNewUserAccount(UserDto userDto);

  User saveUserAccount(User user);

  void deleteUser(User user);

  User getUserByID(int id);

  User getUserByEmail(String email);

  List<User> getExperts();

  List<User> getCandidates();
}
