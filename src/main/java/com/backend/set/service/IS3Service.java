package com.backend.set.service;

import com.amazonaws.services.s3.model.ListObjectsV2Result;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.List;

public interface IS3Service {
  List<String> getAllBucketNames();

  String createBucket(String bucketName);

  StreamingResponseBody getFile(String bucketName, String fileName);

  String readFileAsString(String bucketName, String fileName);

  void putFile(String bucketName, MultipartFile file);

  ListObjectsV2Result getFilesInBucket(String bucketName);

  void deleteFile(String bucketName, String fileName);
}
