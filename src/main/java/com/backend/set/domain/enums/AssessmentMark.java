package com.backend.set.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.stream.Stream;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum AssessmentMark {
  FIVE("5"),
  FOUR("4"),
  THREE("3"),
  TWO("2"),
  ONE("1");

  @Getter
  @Setter
  private String mark;

  @JsonCreator
  public static AssessmentMark decode(final String code) {
    return Stream.of(AssessmentMark.values()).filter(targetEnum -> targetEnum.mark.equals(code)).findFirst().orElse(null);
  }

  @JsonValue
  public String getCode() {
    return mark;
  }
}
