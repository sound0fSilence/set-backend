package com.backend.set.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.stream.Stream;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Role {

  EXPERT("expert"),
  CANDIDATE("candidate");

  @Getter
  @Setter
  private String userRole;

  @JsonCreator
  public static Role fromText(final String text) {
    return Stream.of(Role.values()).filter(targetEnum -> targetEnum.userRole.equals(text)).findFirst().orElse(null);
  }

  @Override
  public String toString() {
    return userRole;
  }
}
