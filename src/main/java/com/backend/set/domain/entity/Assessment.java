package com.backend.set.domain.entity;

import com.backend.set.domain.enums.AssessmentMark;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.With;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@With
public class Assessment {
  @Id
  @GeneratedValue
  private Integer id;

  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany
  List<User> experts;
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
//  @PrimaryKeyJoinColumn
  User candidate;
  @LazyCollection(LazyCollectionOption.FALSE)
  @ElementCollection(targetClass = AssessmentMark.class)
  @CollectionTable(name = "assessment_mark", joinColumns = @JoinColumn(name = "id"))
  List<AssessmentMark> marks;
  @ElementCollection(fetch = FetchType.EAGER)
  List<String> questions;

  String date;

  boolean isActive;
}
