package com.backend.set.domain.entity;

import com.backend.set.domain.enums.Role;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_account")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class User {

  @GeneratedValue(strategy = GenerationType.AUTO)
  @Id
  @Column(name = "userID")
  private Integer id;

  private String firstName;

  private String lastName;

  private String email;

  @Column(length = 60)
  private String password;

  private Role role;

  @Override
  public String toString() {
    return firstName + " " + lastName;
  }
}
