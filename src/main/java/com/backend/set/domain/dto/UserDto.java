package com.backend.set.domain.dto;

import com.backend.set.utils.annotations.PasswordMatches;
import com.backend.set.utils.annotations.ValidEmail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@PasswordMatches
@With
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

  @NotNull
  @NotEmpty
  private String firstName;

  @NotNull
  @NotEmpty
  private String lastName;

  @NotNull
  @NotEmpty
  private String password;
  private String matchingPassword;

  @ValidEmail
  @NotNull
  @NotEmpty
  private String email;

  @NotNull
  private String role;
}
