package com.backend.set.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BucketDto {
  private String bucketName;

  private String fileName;

}
