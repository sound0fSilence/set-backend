package com.backend.set.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SetAssessmentMarksDto {

  private List<Marks> marks;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Marks {
    private int expertId;
    private String expertMark;
  }
}
