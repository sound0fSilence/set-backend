package com.backend.set.controller;

import com.backend.set.config.AssessmentPropertyConfig;
import com.backend.set.domain.dto.AssessmentDto;
import com.backend.set.domain.dto.SetAssessmentMarksDto;
import com.backend.set.domain.entity.Assessment;
import com.backend.set.domain.enums.AssessmentMark;
import com.backend.set.service.IAssessmentService;
import com.backend.set.utils.FrameworkUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/assessments")
@Log4j2
public class AssessmentController {

  @Autowired
  private IAssessmentService assessmentService;

  @Autowired
  private FrameworkUtils frameworkUtils;

  @Autowired
  private AssessmentPropertyConfig assessmentPropertyConfig;

  @Operation(summary = "Start assessment session")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Assessment is started"),
      @ApiResponse(responseCode = "400", description = "No required attendees are available",
          content = @Content)})
  @PostMapping(value = "/start", produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.CREATED)
  public Assessment createAssessment(@Valid @RequestBody final AssessmentDto assessmentDto) {
    log.info("Start new assessment: {}", assessmentDto);
    return assessmentService.startAssessment(assessmentDto);
  }

  @Operation(summary = "Evaluate candidate")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Marks are set",
          content = {@Content(mediaType = "application/json")}),
      @ApiResponse(responseCode = "400", description = "No such assessment found with id",
          content = @Content)})
  @PutMapping("/{assessmentId}/marks")
  public void setAssessmentMarks(@PathVariable int assessmentId, @Valid @RequestBody final SetAssessmentMarksDto marksDto) {
    marksDto.getMarks().forEach(mark -> assessmentService.evaluateCandidate(assessmentId, mark.getExpertId(), AssessmentMark.decode(mark.getExpertMark())));
  }

  @Operation(summary = "Get assessment result")
  @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "400", description = "No such assessment found with id",
          content = @Content)
  })
  @GetMapping("/{assessmentId}/result")
  public String getAssessmentResult(@PathVariable int assessmentId) {
    double mark = assessmentService.getTotalAssessmentMark(assessmentId);
    boolean isPassed = isAssessmentPassed(mark);
    Assessment assessment = assessmentService.getAssessmentById(assessmentId);
    String result = frameworkUtils.getAssessmentResult(isPassed, assessment, mark);
    log.info(result);
    if (isPassed) {
      log.info(String.format("Deleting candidate: %s from candidates list", assessment.getCandidate()));
      assessmentService.deleteCandidate(assessment);
    }
    return result;
  }

  private boolean isAssessmentPassed(double mark) {
    return mark >= assessmentPropertyConfig.getPassedMark();
  }

  @Operation(summary = "Get all assessments")
  @GetMapping
  public List<Assessment> getAllAssessments() {
    return assessmentService.getAllAssessments();
  }

  @Operation(summary = "Get assessment questions")
  @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "400", description = "No such assessment found with id",
          content = @Content)
  })
  @GetMapping("/{assessmentId}/questions")
  public List<String> getAssessmentQuestions(@PathVariable int assessmentId) {
    return assessmentService.getQuestionList(assessmentId);
  }

  @Operation(summary = "Get assessment random question")
  @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "400", description = "No such assessment found with id",
          content = @Content)
  })
  @GetMapping("/{assessmentId}/questions/random")
  public String getRandomQuestion(@PathVariable int assessmentId) {
    String question = assessmentService.askRandomQuestion(assessmentId);
    log.info("Question to ask: " + question);
    return question;
  }
}
