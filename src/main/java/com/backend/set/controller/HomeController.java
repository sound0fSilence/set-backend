package com.backend.set.controller;

import com.backend.set.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class HomeController {

  @Autowired
  private UserService userService;

  @GetMapping
  public String main(Map<String, Object> model) {
    return "home";
  }
}
