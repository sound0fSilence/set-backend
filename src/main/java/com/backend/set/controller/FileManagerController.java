package com.backend.set.controller;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.backend.set.domain.dto.BucketDto;
import com.backend.set.service.IAssessmentService;
import com.backend.set.service.IS3Service;
import com.backend.set.utils.annotations.ValidFileExtension;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.List;

@Validated
@RestController
@RequestMapping("/api/file-manager")
public class FileManagerController {

  @Autowired
  private IS3Service s3Service;

  @Autowired
  IAssessmentService assessmentService;

  @Operation(summary = "Get all buckets")
  @ApiResponses(@ApiResponse(responseCode = "200", description = "OK"))
  @GetMapping("/bucket/all")
  public List<String> getAllBuckets() {
    return s3Service.getAllBucketNames();
  }

  @Operation(summary = "Get all files in a bucket")
  @ApiResponses(@ApiResponse(responseCode = "200", description = "OK"))
  @GetMapping("/file/all")
  public List<S3ObjectSummary> getAllFiles(@RequestParam String bucket) {
    return s3Service.getFilesInBucket(bucket).getObjectSummaries();
  }

  @Operation(summary = "Create a new bucket")
  @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Bucket is created")})
  @PostMapping("/bucket")
  @ResponseStatus(HttpStatus.CREATED)
  public String createBucket(@RequestParam String bucketName) {
    return s3Service.createBucket(bucketName);
  }

  @Operation(summary = "Upload a new file", description = "Upload json file with questions")
  @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "File is created"),
      @ApiResponse(responseCode = "400", description = "Invalid file extension")})
  @PostMapping(value = "/file", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
  @ResponseStatus(HttpStatus.CREATED)
  public String handleFileUpload(@ValidFileExtension @RequestParam(name = "file") MultipartFile sourceFile) {
    s3Service.putFile("uploads-questions", sourceFile);
    return sourceFile.getOriginalFilename();
  }

  @Operation(summary = "Download a file")
  @ApiResponses(@ApiResponse(responseCode = "200", description = "OK"))
  @GetMapping(value = "/download/file", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
  public ResponseEntity<StreamingResponseBody> downloadFile(@RequestParam("bucketName") String bucketName,
                                                            @RequestParam("path") String path) {
    return ResponseEntity
        .ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + path + "\"")
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .body(s3Service.getFile(bucketName, path));
  }

  @Operation(summary = "Delete a file")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "400", description = "No file found"),
      @ApiResponse(responseCode = "204", description = "File is deleted")})
  @DeleteMapping("/delete/file")
  public BucketDto deleteByKey(@RequestBody BucketDto bucketDto) {
    s3Service.deleteFile(bucketDto.getBucketName(), bucketDto.getFileName());
    return bucketDto;
  }

  @Operation(summary = "Read an uploaded file")
  @ApiResponses(@ApiResponse(responseCode = "200", description = "OK"))
  @GetMapping(value = "/upload/file")
  public String readUploadedFile(@RequestParam("bucketName") String bucketName, @RequestParam("fileName") String fileName) {
    return s3Service.readFileAsString(bucketName, fileName);
  }

  @Operation(summary = "Add assessment questions", description = "Add questions from file to assessment questions list")
  @ApiResponses(@ApiResponse(responseCode = "200", description = "OK"))
  @GetMapping(value = "/upload/addQuestionList")
  public void addQuestionsToAssessmentFromFile(@RequestParam("bucketName") String bucketName, @RequestParam("fileName") String fileName) {
    String questions = s3Service.readFileAsString(bucketName, fileName);
    assessmentService.addQuestionList(questions);
    s3Service.deleteFile(bucketName, fileName);
  }
}