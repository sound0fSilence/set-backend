package com.backend.set.controller;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;
import com.backend.set.service.IUserService;
import com.backend.set.utils.mapper.UserMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/users")
@Log4j2
public class UserController {

  @Autowired
  private IUserService userService;
  @Autowired
  private UserMapper userMapper;

  @Operation(summary = "Register new user")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "User is created",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = User.class))}),
      @ApiResponse(responseCode = "400", description = "There is an account with email address",
          content = @Content)})
  @PostMapping(value = "/registration", produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.CREATED)
  public User registerUserAccount(@Valid @RequestBody final UserDto userDto) {
    log.info("Registering user account with information: {}", userDto);

    return userService.registerNewUserAccount(userDto);
  }

  @Operation(summary = "Update user")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "User is updated successfully",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = User.class))}),
      @ApiResponse(responseCode = "400", description = "No user found with id",
          content = @Content)})
  @PutMapping("/{userId}")
  public User updateUserAccount(@PathVariable(value = "userId") int userId, @Valid @RequestBody final UserDto userDto) {
    log.info("Updated user account with information: {}", userDto);
    User user = userService.getUserByID(userId);
    user = userMapper.updateUserFromDto(user, userDto);
    return userService.saveUserAccount(user);
  }

  @Operation(summary = "Delete user")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "400", description = "No user found with id"),
      @ApiResponse(responseCode = "204", description = "User is deleted")})
  @DeleteMapping("/{userId}")
  public ResponseEntity<Object> deleteUserAccount(@PathVariable(value = "userId") int userId) {
    log.info("Delete user account with id: {}", userId);
    if (Objects.isNull(userService.getUserByID(userId))) {
      return ResponseEntity.status(HttpStatus.OK).build();
    }
    User user = userService.getUserByID(userId);
    userService.deleteUser(user);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Get list of experts for assessment session")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Candidates found")})
  @GetMapping("/experts")
  public List<User> getAvailableExperts() {
    return userService.getExperts();
  }

  @Operation(summary = "Get list of candidates for assessment session")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Candidates found")})
  @GetMapping("/candidates")
  public List<User> getCandidates() {
    return userService.getCandidates();
  }
}
