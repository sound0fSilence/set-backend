package com.backend.set.utils;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public interface IJsonUtils {

  static <T> T fromJson(final String json, final Class<T> returnType) {
    return new Gson().fromJson(json, returnType);
  }

  static <T> T fromJson(final String json, final Type type) {
    return new Gson().fromJson(json, type);
  }

  static String toJson(final Object o) {
    return new Gson().newBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(o);
  }
}
