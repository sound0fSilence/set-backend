package com.backend.set.utils.validation;


import com.backend.set.utils.annotations.ValidFileExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class FileExtensionValidator implements ConstraintValidator<ValidFileExtension, MultipartFile> {

  private static final String EMAIL_PATTERN = "^.*.json$";

  @Override
  public void initialize(ValidFileExtension constraintAnnotation) {
    //override this method with custom parameter type
  }

  @Override
  public boolean isValid(@RequestBody MultipartFile file, ConstraintValidatorContext context) {
    return (validateFileExtension(file.getOriginalFilename()));
  }

  private boolean validateFileExtension(String fileName) {
    Pattern pattern = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
    Matcher matcher = pattern.matcher(fileName);
    return matcher.matches();
  }
}
