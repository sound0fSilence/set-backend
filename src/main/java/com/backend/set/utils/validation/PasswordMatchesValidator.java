package com.backend.set.utils.validation;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.utils.annotations.PasswordMatches;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

  @Override
  public void initialize(PasswordMatches constraintAnnotation) {
    //override this method with custom parameter type
  }

  @Override
  public boolean isValid(Object obj, ConstraintValidatorContext context) {
    UserDto user = (UserDto) obj;
    return user.getPassword().equals(user.getMatchingPassword());
  }
}