package com.backend.set.utils.mapper;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.utils.annotations.Utils;


@Utils
public class UserMapper {

  public User dtoToEntity(UserDto userDto) {
    final User user = new User();
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setPassword(userDto.getPassword());
    user.setEmail(userDto.getEmail());
    user.setRole(Role.valueOf(userDto.getRole().toUpperCase()));
    return user;
  }

  public User updateUserFromDto(User user, UserDto userDto) {
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setPassword(userDto.getPassword());
    user.setEmail(userDto.getEmail());
    user.setRole(Role.valueOf(userDto.getRole().toUpperCase()));
    return user;
  }
}
