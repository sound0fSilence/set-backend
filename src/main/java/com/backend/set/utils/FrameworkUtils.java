package com.backend.set.utils;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.Assessment;
import com.backend.set.domain.enums.Role;
import com.backend.set.utils.annotations.Utils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Utils
public class FrameworkUtils {

  public String getAssessmentResult(boolean isPassed, Assessment assessment, double finalMark) {
    String result = isPassed ?
        "Passed.\nCongrats on promotion!" : "Not passed.\nKeep working on your way to excellence!";
    assessment.setActive(false);
    return String.format("""
            Assessment mark for candidate %s is %s
            Result is %s
            """
        , assessment.getCandidate(), finalMark, result);
  }

  @SneakyThrows
  public List<String> getQuestionList() {
    List<String> questionList = new ArrayList<>();
    new ObjectMapper().readValue(new File("src/main/resources/questions.json"), JsonNode.class)
        .elements().forEachRemaining(entry -> questionList.add(entry.get("question").asText()));
    return questionList;
  }

  @SneakyThrows
  public UserDto getRandomUserDtoWithRole(Role role) {
    int id = ThreadLocalRandom.current().nextInt(0, 1000);
    UserDto userDto = new ObjectMapper().readValue(new File("src/main/resources/user.json"), UserDto.class);
    return userDto.withEmail(id + userDto.getEmail()).withRole(role.getUserRole());
  }
}
