package com.backend.set;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Log4j2
public class AssessmentApplication {

  @Autowired
  private AWSCredentialsProvider awsCredentialsProvider;

  @Bean
  public AWSCredentialsProvider awsCredentialsProvider() {
    return new DefaultAWSCredentialsProviderChain();
  }

  @Bean
  public AmazonS3 amazonS3() {
    AmazonS3ClientBuilder builder;
    log.info("Creating Configuring S3 connection using IAM credentials");
    builder = AmazonS3ClientBuilder.standard()
        .withCredentials(awsCredentialsProvider)
        .withRegion(Regions.US_EAST_2);
    return builder.build();
  }

  public static void main(String[] args) {
    SpringApplication.run(AssessmentApplication.class, args);
  }

}
