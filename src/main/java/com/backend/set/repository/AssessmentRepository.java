package com.backend.set.repository;

import com.backend.set.domain.entity.Assessment;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

public interface AssessmentRepository extends JpaRepository<Assessment, Integer> {

  @Override
  @RestResource(exported = false)
  List<Assessment> findAll();

  @Override
  @RestResource(exported = false)
  List<Assessment> findAll(Sort sort);

  @Override
  @RestResource(exported = false)
  List<Assessment> findAllById(Iterable<Integer> integers);

  @Override
  @RestResource(exported = false)
  <S extends Assessment> List<S> saveAll(Iterable<S> entities);

  @Override
  @RestResource(exported = false)
  <S extends Assessment> S save(S entity);

  @Override
  @RestResource(exported = false)
  void deleteById(Integer integer);

  @Override
  @RestResource(exported = false)
  void delete(Assessment entity);

  @Override
  @RestResource(exported = false)
  void deleteAllById(Iterable<? extends Integer> integers);

  @Override
  @RestResource(exported = false)
  void deleteAll(Iterable<? extends Assessment> entities);

  @Override
  @RestResource(exported = false)
  void deleteAll();
}
