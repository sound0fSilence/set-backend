package com.backend.set.repository;

import com.backend.set.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

public interface UserRepository extends JpaRepository<User, Integer> {

  User findByEmail(@Param("email") String email);

  @Override
  @RestResource(exported = false)
  void delete(User user);

  @Override
  @RestResource(exported = false)
  <S extends User> S save(S entity);

  @Override
  @RestResource(exported = false)
  void deleteById(Integer integer);

  @Override
  @RestResource(exported = false)
  void deleteAllById(Iterable<? extends Integer> integers);

  @Override
  @RestResource(exported = false)
  void deleteAll(Iterable<? extends User> entities);

  @Override
  @RestResource(exported = false)
  void deleteAll();
}
