package com.backend.set.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Configuration
@PropertySource("classpath:assessment.properties")
public class AssessmentPropertyConfig {

  @Value("${passed.mark}")
  private double passedMark;

  @Value("${required.mark.qty}")
  private int requiredMarkQty;


  public void setRequiredMarkQty(int requiredMarkQty) {
    this.requiredMarkQty = requiredMarkQty;
  }
}
