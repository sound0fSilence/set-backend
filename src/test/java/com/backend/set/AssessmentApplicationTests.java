package com.backend.set;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(args = {"--DATABASE_HOST=localhost", "--DATABASE_PORT=3306", "--DATABASE_NAME=assessment",
    "--DATABASE_USER=admin", "--DATABASE_PASSWORD=admin"})
class AssessmentApplicationTests {

  @Test
  void contextLoads() {
  }

}
