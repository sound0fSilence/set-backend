package com.backend.set.e2e;


import com.backend.set.controller.AssessmentController;
import com.backend.set.controller.UserController;
import com.backend.set.domain.dto.AssessmentDto;
import com.backend.set.domain.dto.SetAssessmentMarksDto;
import com.backend.set.domain.entity.Assessment;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.service.impl.AssessmentService;
import com.backend.set.utils.FrameworkUtils;
import com.backend.set.utils.mapper.UserMapper;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.backend.set.domain.dto.SetAssessmentMarksDto.Marks;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AssessmentFlowTest {

  @Autowired
  private AssessmentController assessmentController;

  @Autowired
  private AssessmentService assessmentService;

  @Autowired
  private UserController userController;

  @Autowired
  private FrameworkUtils frameworkUtils;

  @Autowired
  private UserMapper userMapper;

  @Test
  public void fullAssessmentFlowTest() {
    //prepare users for assessment
    User firstExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    User secondExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    User candidate = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));
    //start assessment
    Assessment assessment = assessmentController.createAssessment(new AssessmentDto("11.12.21"));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(assessment.getCandidate()).as("Assessment does not include candidate")
        .isEqualTo(candidate);
    softly.assertThat(assessment.getExperts()).as("Assessment does not include experts")
        .containsAll(List.of(firstExpert, secondExpert));
    softly.assertThat(assessment.getMarks()).as("Assessment has mark before evaluation").isNull();
    softly.assertThat(assessment.getDate()).as("Assessment does not have date").isNotEmpty();
    softly.assertAll();
    //get assessment question list
    softly.assertThat(assessmentController.getAssessmentQuestions(assessment.getId()))
        .as("Assessment does not include questions").hasSize(25);
    //ask assessment question
    String randomQuestion = assessmentController.getRandomQuestion(assessment.getId());
    softly.assertThat(randomQuestion).as("Question is empty").isNotEmpty();
    softly.assertThat(assessmentController.getAssessmentQuestions(assessment.getId()))
        .as("Question list has not changed after question").hasSize(24);
    softly.assertAll();

    //evaluate candidate
    List<Marks> evaluationMarks = List.of(
        new Marks(firstExpert.getId(), "4"),
        new Marks(secondExpert.getId(), "4"));
    assessmentController.setAssessmentMarks(assessment.getId(), new SetAssessmentMarksDto(evaluationMarks));
    softly.assertThat(assessmentService.getAssessmentById(assessment.getId()).getMarks())
        .as("Assessment does not include assessment marks after evaluation").isNotEmpty();

    //get assessment result
    String result = assessmentController.getAssessmentResult(assessment.getId());
    softly.assertThat(result).as("Assessment result is not es expected").contains("Congrats on promotion!");
    softly.assertAll();
  }
}
