package com.backend.set.suites;

import com.backend.set.controller.AssessmentControllerIntegrationTest;
import com.backend.set.controller.UserControllerIntegrationTest;
import com.backend.set.controller.UserControllerUnitTest;
import com.backend.set.database.DatastoreSystemsHealthTest;
import com.backend.set.e2e.AssessmentFlowTest;
import com.backend.set.repository.UserRepositoryIntegrationTest;
import com.backend.set.service.UserServiceIntegrationTest;
import com.backend.set.service.UserServiceUnitTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    AssessmentControllerIntegrationTest.class,
    UserControllerIntegrationTest.class,
    UserRepositoryIntegrationTest.class,
    UserServiceIntegrationTest.class,
    UserControllerUnitTest.class,
    UserServiceUnitTest.class,
    AssessmentFlowTest.class,
    DatastoreSystemsHealthTest.class
})
public class AllTestSuite {
}

