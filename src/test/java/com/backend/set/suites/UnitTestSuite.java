package com.backend.set.suites;

import com.backend.set.controller.UserControllerUnitTest;
import com.backend.set.service.UserServiceUnitTest;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SuiteDisplayName;

@SelectClasses({
    UserControllerUnitTest.class,
    UserServiceUnitTest.class,
})
@org.junit.platform.suite.api.Suite
@SuiteDisplayName("Unit tests")
public class UnitTestSuite {
}
