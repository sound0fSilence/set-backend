package com.backend.set.suites;

import com.backend.set.controller.AssessmentControllerIntegrationTest;
import com.backend.set.controller.UserControllerIntegrationTest;
import com.backend.set.database.DatastoreSystemsHealthTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    DatastoreSystemsHealthTest.class,
    UserControllerIntegrationTest.class,
    AssessmentControllerIntegrationTest.class
})
public class ContinuousIntegrationTestSuite {
  //intentionally empty - Test Suite Setup (annotations) is sufficient
}
