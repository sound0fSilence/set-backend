package com.backend.set.controller;

import com.backend.set.domain.dto.AssessmentDto;
import com.backend.set.domain.dto.SetAssessmentMarksDto;
import com.backend.set.domain.entity.Assessment;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.service.impl.AssessmentService;
import com.backend.set.utils.FrameworkUtils;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AssessmentControllerIntegrationTest {

  @Autowired
  private AssessmentController assessmentController;

  @Autowired
  private AssessmentService assessmentService;

  @Autowired
  private UserController userController;

  @Autowired
  private FrameworkUtils frameworkUtils;

  @Test
  public void startAssessmentTest() {
    User firstExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    User secondExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    User candidate = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));
    Assessment assessment = assessmentController.createAssessment(new AssessmentDto("11.12.21"));

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(assessment.getCandidate()).as("Assessment does not include candidate")
        .isEqualTo(candidate);
    softly.assertThat(assessment.getExperts()).as("Assessment does not include experts")
        .containsAll(List.of(firstExpert, secondExpert));
    softly.assertThat(assessment.getMarks()).as("Assessment has mark before evaluation").isNull();
    softly.assertThat(assessment.getDate()).as("Assessment does not have date").isNotEmpty();
    softly.assertAll();
  }

  @Test
  public void getAssessmentQuestionsTest() {
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));
    Assessment assessment = assessmentController.createAssessment(new AssessmentDto("11.12.21"));
    Assertions.assertThat(assessmentController.getAssessmentQuestions(assessment.getId()))
        .as("Assessment does not include questions").hasSize(25);
  }

  @Test
  public void getAssessmentRandomQuestionTest() {
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));
    Assessment assessment = assessmentController.createAssessment(new AssessmentDto("11.12.21"));

    SoftAssertions softly = new SoftAssertions();
    String randomQuestion = assessmentController.getRandomQuestion(assessment.getId());
    softly.assertThat(randomQuestion).as("Question is empty").isNotEmpty();
    softly.assertThat(assessmentController.getAssessmentQuestions(assessment.getId()))
        .as("Question list has not changed after question").hasSize(24);
    softly.assertAll();
  }

  @Test
  public void evaluateCandidateTest() {
    User firstExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    User secondExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));
    Assessment assessment = assessmentController.createAssessment(new AssessmentDto("11.12.21"));

    //evaluate candidate
    List<SetAssessmentMarksDto.Marks> evaluationMarks = List.of(
        new SetAssessmentMarksDto.Marks(firstExpert.getId(), "4"),
        new SetAssessmentMarksDto.Marks(secondExpert.getId(), "4"));
    assessmentController.setAssessmentMarks(assessment.getId(), new SetAssessmentMarksDto(evaluationMarks));
    Assertions.assertThat(assessmentService.getAssessmentById(assessment.getId()).getMarks())
        .as("Assessment does not include assessment marks after evaluation").isNotEmpty();
  }

  @Test
  public void getAssessmentResult() {
    User firstExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    User secondExpert = userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    userController.registerUserAccount(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));
    Assessment assessment = assessmentController.createAssessment(new AssessmentDto("11.12.21"));

    //evaluate candidate
    List<SetAssessmentMarksDto.Marks> evaluationMarks = List.of(
        new SetAssessmentMarksDto.Marks(firstExpert.getId(), "4"),
        new SetAssessmentMarksDto.Marks(secondExpert.getId(), "4"));
    assessmentController.setAssessmentMarks(assessment.getId(), new SetAssessmentMarksDto(evaluationMarks));
    //get assessment result
    String result = assessmentController.getAssessmentResult(assessment.getId());
    Assertions.assertThat(result).as("Assessment result is not es expected").contains("Congrats on promotion!");
  }
}
