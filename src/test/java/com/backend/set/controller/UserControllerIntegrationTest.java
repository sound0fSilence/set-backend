package com.backend.set.controller;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.utils.FrameworkUtils;
import com.backend.set.utils.exception.UserException;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerIntegrationTest {
  @Autowired
  private UserController userController;
  @Autowired
  private FrameworkUtils frameworkUtils;

  @Test
  public void testRegisterUserAccount() {
    UserDto userDto = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);

    User actualUser = userController.registerUserAccount(userDto);

    SoftAssertions assertions = new SoftAssertions();
    assertions.assertThat(actualUser).as("User does not exist" + userDto).isNotNull();
    assertions.assertThat(actualUser.getRole()).as("User role does not match")
        .isEqualTo(Role.fromText(userDto.getRole()));
    assertions.assertThat(actualUser.getFirstName()).as("User first name does not match")
        .isEqualTo(userDto.getFirstName());
    assertions.assertThat(actualUser.getLastName()).as("User last name does not match")
        .isEqualTo(userDto.getLastName());
    assertions.assertThat(actualUser.getEmail()).as("User email does not match")
        .isEqualTo(userDto.getEmail());
    assertions.assertThat(actualUser.getPassword()).as("User password does not match")
        .isEqualTo(userDto.getPassword());
    assertions.assertAll();
  }

  @Test
  public void testExceptionWhenRegisterUserWithExistingEmail() {
    UserDto user = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);
    userController.registerUserAccount(user);
    Exception exception = assertThrows(UserException.class, () -> userController.registerUserAccount(user));
    assertThat(exception.getMessage()).as("Exception message doesn't match")
        .contains("There is an account with that email address: " + user.getEmail());
  }
}
