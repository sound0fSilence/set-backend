package com.backend.set.controller;

import com.backend.set.AssessmentApplication;
import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.service.impl.UserService;
import com.backend.set.utils.FrameworkUtils;
import com.backend.set.utils.IJsonUtils;
import com.backend.set.utils.mapper.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = {AssessmentApplication.class})
public class UserControllerUnitTest {
  private MockMvc mockMvc;

  @MockBean
  private UserService userService;

  @InjectMocks
  private UserController userController;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Autowired
  private FrameworkUtils frameworkUtils;

  @Autowired
  private UserMapper userMapper;

  @Before
  public void setUp() {
    this.mockMvc = webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void testRegisterUserAccount() throws Exception {
    // setup mock user returned the mock service component
    UserDto userDto = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);

    User mockUser = userMapper.dtoToEntity(userDto);

    when(userService.registerNewUserAccount(any(UserDto.class))).thenReturn(mockUser);

    // simulate the form bean that would POST from the web page
    UserDto user = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);


    // simulate the form submit (POST)
    mockMvc.perform(post("/users/registration")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(IJsonUtils.toJson(user)))
        .andExpect(status().isCreated())
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(mockUser.getEmail()))
        .andReturn();
  }

  @Test
  public void testAddUserWithInvalidEmail() throws Exception {

    // simulate the form bean that would POST from the web page
    UserDto user = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);
    user.setEmail("invalidEmail");
    // simulate the form submit (POST)
    mockMvc.perform(post("/users/registration")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(IJsonUtils.toJson(user)))
        .andExpect(status().is(400))
        .andExpect(result -> assertThat(Objects.requireNonNull(result.getResolvedException()).getMessage())
            .contains("Invalid email"))
        .andReturn();
  }

  @Test
  public void testAddUserWithInvalidPassword() throws Exception {

    // simulate the form bean that would POST from the web page
    UserDto userDto = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);
    userDto.setMatchingPassword("nonMatchedPassword");
    // simulate the form submit (POST)
    mockMvc.perform(post("/users/registration")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(IJsonUtils.toJson(userDto)))
        .andExpect(status().is(400))
        .andExpect(result -> assertThat(Objects.requireNonNull(result.getResolvedException()).getMessage())
            .contains("Passwords don't match"))
        .andReturn();
  }

}
