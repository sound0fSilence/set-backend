package com.backend.set.repository;

import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.utils.FrameworkUtils;
import com.backend.set.utils.mapper.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryIntegrationTest {

  @Autowired
  private TestEntityManager entityManager;
  @Autowired
  private UserRepository userRepository;
  private FrameworkUtils frameworkUtils;
  private UserMapper userMapper;

  @Before
  public void setUp() {
    frameworkUtils = new FrameworkUtils();
    userMapper = new UserMapper();
  }

  @Test
  public void testFindByEmail() {
    User user = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));

    entityManager.persist(user);

    User foundUser = userRepository.findByEmail(user.getEmail());

    assertThat(foundUser.getEmail(), is(equalTo(user.getEmail())));
  }
}
