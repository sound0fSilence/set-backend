package com.backend.set.service;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.utils.FrameworkUtils;
import com.backend.set.utils.mapper.UserMapper;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class UserServiceIntegrationTest {

  @Autowired
  private IUserService userService;
  @Autowired
  private FrameworkUtils frameworkUtils;
  @Autowired
  private UserMapper userMapper;

  @Test
  public void testAddUserHappyPath() {
    UserDto user = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);
    User actualUser = userService.registerNewUserAccount(user);
    SoftAssertions assertions = new SoftAssertions();
    assertions.assertThat(actualUser).as("User does not exist").isNotNull();
    assertions.assertThat(actualUser.getRole().toString()).as("User role does not match")
        .isEqualTo(user.getRole());
    assertions.assertThat(actualUser.getFirstName()).as("User first name does not match")
        .isEqualTo(user.getFirstName());
    assertions.assertThat(actualUser.getLastName()).as("User last name does not match")
        .isEqualTo(user.getLastName());
    assertions.assertThat(actualUser.getEmail()).as("User email does not match")
        .isEqualTo(user.getEmail());
    assertions.assertThat(actualUser.getPassword()).as("User password does not match")
        .isEqualTo(user.getPassword());
    assertions.assertAll();

  }

}
