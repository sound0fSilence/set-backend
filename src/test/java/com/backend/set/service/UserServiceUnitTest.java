package com.backend.set.service;

import com.backend.set.domain.dto.UserDto;
import com.backend.set.domain.entity.User;
import com.backend.set.domain.enums.Role;
import com.backend.set.repository.UserRepository;
import com.backend.set.service.impl.UserService;
import com.backend.set.utils.FrameworkUtils;
import com.backend.set.utils.mapper.UserMapper;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserServiceUnitTest {

  @InjectMocks
  private UserService userService;

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private FrameworkUtils frameworkUtils;

  @Mock
  private UserMapper mockUserMapper;

  private UserMapper userMapper;

  @Before
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    userMapper = new UserMapper();
  }

  @Test
  public void addUserHappyPathTest() {
    User mockUser = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    UserDto testDto = frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT);

    when(userRepository.save(any(User.class))).thenReturn(mockUser);
    when(mockUserMapper.dtoToEntity(testDto)).thenReturn(mockUser);

    User newUser = userService.registerNewUserAccount(testDto);


    verify(userRepository, Mockito.times(1)).save(any(User.class));

    SoftAssertions assertions = new SoftAssertions();
    assertions.assertThat(newUser.getId()).as("User id does not match")
        .isEqualTo(mockUser.getId());
    assertions.assertThat(Role.EXPERT).as("User role does not match")
        .isEqualTo(mockUser.getRole());
    assertions.assertThat(newUser.getFirstName()).as("User first name does not match")
        .isEqualTo(mockUser.getFirstName());
    assertions.assertThat(newUser.getLastName()).as("User last name does not match")
        .isEqualTo(mockUser.getLastName());
    assertions.assertThat(newUser.getEmail()).as("User email does not match")
        .isEqualTo(mockUser.getEmail());
    assertions.assertThat(newUser.getPassword()).as("User password does not match")
        .isEqualTo(mockUser.getPassword());
    assertions.assertAll();
  }

  @Test
  public void getUserByEmailTest() {
    User mockUser = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));

    when(userRepository.findByEmail(mockUser.getEmail())).thenReturn(mockUser);

    User newUser = userService.getUserByEmail(mockUser.getEmail());

    verify(userRepository, Mockito.times(1)).findByEmail(mockUser.getEmail());

    SoftAssertions assertions = new SoftAssertions();
    assertions.assertThat(mockUser.getFirstName()).as("User first name does not match")
        .isEqualTo(newUser.getFirstName());
    assertions.assertThat(mockUser.getLastName()).as("User last name does not match")
        .isEqualTo(newUser.getLastName());
    assertions.assertThat(mockUser.getEmail()).as("User email does not match").isEqualTo(newUser.getEmail());
    assertions.assertThat(Role.EXPERT).as("User role does not match").isEqualTo(mockUser.getRole());
    assertions.assertThat(mockUser.getPassword()).as("User password does not match")
        .isEqualTo(newUser.getPassword());
    assertions.assertAll();

  }

  @Test
  public void deleteUserTest() {
    User mockUser = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    Mockito.doNothing().when(userRepository).delete(mockUser);
    userService.deleteUser(mockUser);
    Mockito.verify(userRepository, Mockito.times(1)).delete(mockUser);
  }

  @Test
  public void getExpertsTest() {
    User mockFirstExpert = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));
    User mockSecondExpert = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.EXPERT));

    when(userService.getExperts()).thenReturn(List.of(mockFirstExpert, mockSecondExpert));

    List<User> experts = userService.getExperts();
    Mockito.verify(userRepository, Mockito.times(1)).findAll();
    assertThat(experts.size()).as("Experts counts ar not as expected").isEqualTo(2);
    assertThat(experts.containsAll(List.of(mockFirstExpert, mockSecondExpert)))
        .as("Experts list does not contain experts").isTrue();
  }

  @Test
  public void getCandidatesTest() {
    User mockFirstCandidate = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));
    User mockSecondCandidate = userMapper.dtoToEntity(frameworkUtils.getRandomUserDtoWithRole(Role.CANDIDATE));

    when(userService.getCandidates()).thenReturn(List.of(mockFirstCandidate, mockSecondCandidate));

    List<User> candidates = userService.getCandidates();
    Mockito.verify(userRepository, Mockito.times(1)).findAll();
    assertThat(candidates.size()).as("Candidates counts ar not as expected").isEqualTo(2);
    assertThat(candidates.containsAll(List.of(mockFirstCandidate, mockSecondCandidate)))
        .as("Candidates list does not contain candidates").isTrue();
  }

}
