FROM openjdk:17-alpine
COPY target/set-0.0.1-SNAPSHOT.jar /
ENTRYPOINT ["java", "-jar", "set-0.0.1-SNAPSHOT.jar"]