# Welcome to Assessment backend application :)

## This application emulates Assessment flow in the company

### Assessment requires at least two experts and one candidate.

**Pre requirements:**

- import postman collection from /postman/Assessment.postman_collection.json file
- import postman environment settings from /postman/assessment_local_environment.json file

**To run the application execute following:**

1. run the application from src/main/java/com/backend/set/AssessmentApplication.java
2. In postman execute requests from 'Assessment flow' folder

Also, there are a bit more requests. Please see postman collection root folder.
[![Build Status](http://9d2d-178-151-139-203.ngrok.io/view/Assessment-APP/job/Assessment_CI/badge/icon)](http://9d2d-178-151-139-203.ngrok.io/view/Assessment-APP/job/Assessment_CI/)
